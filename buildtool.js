/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */
/**
 * Quick build tool to copy files and cleanup
 */

const fs = require("fs");
const rimraf = require("rimraf");
const cla = require("command-line-args");

const options = cla([
    { name: "options", type: String, multiple: true, defaultOption: true, defaultValue: "prebuild" }
]);

const fileCopy = [
    ["./src/index.html", "./dist/index.html"],
    ["./src/style.css", "./dist/style.css"]
];

if (options.options.includes("prebuild"))
    prebuild();
else if (options.options.includes("cleanup"))
    cleanup();

function prebuild() {
    console.log("\n\n----- Copy Files -----");
    for (let i = 0; i < fileCopy.length; i++) {
        const file = fileCopy[i];
        console.log(file);
        fs.copyFile(file[0], file[1], (err) => {
            if (err) throw err;
        });
    }
}

function cleanup() {
    console.log("\n\n----- Cleanup -----");
    rimraf("./dist", (err) => {
        if (err) throw err;
    });
}
