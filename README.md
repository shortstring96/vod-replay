# VOD Replay

VOD Replay mimics Twitch VOD playback using local files or YouTube instead of twitch itself, allowing you to watch VODs with chat offline or after the VOD gets deleted.

## Usage

This program is intended to play videos in sync with an archive of twitch chat, usually in a form of a JSON file.

You can download VODs using [yt-dlp](https://github.com/yt-dlp/yt-dlp) and Chat using [ChatFetch](https://gitlab.com/shortstring96/chatfetch). Also compatible with chat files from [TwitchDownloader](https://github.com/lay295/TwitchDownloader) by lay295.

## Builds

You can get build artifacts from the [pipelines](https://gitlab.com/shortstring96/vod-replay/-/pipelines).
