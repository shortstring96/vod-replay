export type t_chatQueue = {
    offset: number;
    username: string;
    userColor: string;
    badges: string;
    message: string;
    id: string;
};
