/**
 * Types for DownMediaDownloader comments file V1
 */

export type type_commentsV1 = {
    "streamer": {
        "id": string;
    };
    "comments": ({
        "_id": string;
        "created_at": string;
        "updated_at": string;
        "channel_id": string;
        "content_type": string;
        "content_id": string;
        "content_offset_seconds": number;
        "commenter": {
            "display_name": string;
            "_id": string;
            "name": string;
            "type": string;
            "bio": string | null;
            "created_at": string;
            "updated_at": string;
            "logo": string;
        };
        "source": string;
        "state": string;
        "message": {
            "body": string;
            "bits_spent"?: number;
            "fragments": {
                "text": string;
                "emoticon"?: {
                    "emoticon_id": string;
                    "emoticon_set_id": string;
                };
            }[];
            "is_action": boolean;
            "user_badges"?: {
                "_id"?: string;
                "version"?: string;
            }[];
            "user_color": string;
            "user_notice_params": {
                "msg-id"?: string;
                "msg-param-gift-months"?: string;
                "msg-param-months"?: string;
                "msg-param-origin-id"?: string;
                "msg-param-recipient-display-name"?: string;
                "msg-param-recipient-id"?: string;
                "msg-param-recipient-user-name"?: string;
                "msg-param-sender-count"?: string;
                "msg-param-sub-plan"?: string;
                "msg-param-sub-plan-name"?: string;
            };
        };
    })[];
    "resources": {
        "badges"?: {
            [id: string]: string;
        };
        "cheermotes"?: {
            [id: string]: { "data": string };
        };
        "emotes"?: {
            "firstParty": {
                [id: string]: { "data": string; };
            };
            "thirdParty": {
                [code: string]: { "data": string; "format": string; };
            };
        };
    };
};