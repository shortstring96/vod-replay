/**
 * Types for lay295's TwitchDownloader comments file
 */

import { type_commentsV1 } from "./commentsV1";

export type type_comments_TwitchDownloader = {
    "streamer": type_commentsV1["streamer"];
    "comments": type_commentsV1["comments"];
    "video": {
        "start": number;
        "end": number;
    };
    "emotes"?: {
        "thirdParty": {
            "id": string;
            "imageScale": number;
            "data": string;
            "name": string;
        }[];
        "firstParty": {
            "id": string;
            "imageScale": number;
            "data": string;
        }[];
    };
};
