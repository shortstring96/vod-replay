export type t_badgeJSON = {
    badge_sets: {
        [id: string]: {
            versions: {
                [version: number]: {
                    "image_url_1x": string;
                    "image_url_2x": string;
                    "image_url_4x": string;
                };
            };
        };
    };
};
