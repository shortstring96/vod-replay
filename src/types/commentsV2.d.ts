/**
 * Types for ChatV2 from ChatFetch
 * 
 * These are incomplete, only including what should be needed
 */

export type type_commentsV2 = {
    "id": string,
    "creator": {
        "id": string
    },
    "comments": {
        "edges": {
            "node": {
                "commenter": {
                    "displayName": string,
                    "id": string,
                    "login": string,
                },
                "contentOffsetSeconds": number,
                "message": {
                    "fragments": {
                        "emote"?: {
                            "emoteID": string,
                            "from": number,
                            "id": string,
                        },
                        "text": string,
                    }[],
                    "userBadges": {
                        "id": string,
                        "setID": string,
                        "version": string,
                    }[]
                    "userColor"?: string,
                }
            }
        }[]
    },
    "resources"?: {
        "badges": [string, string][],
        "emotes": [string, string][],
        "emotes_bttv": [string, string][],
    }
}
