import { OpenDialogOptions } from "electron";
import { loadBadge, loadCheer, loadFirstPartyEmote, loadThirdPartyEmote } from "./loadResources";
import { type_commentsV1 } from "./types/commentsV1";
import { readFileSync } from "fs";
import { type_commentsV2 } from "./types/commentsV2";

// Video location in seconds
// eslint-disable-next-line prefer-const
let playerTime = 0;

$(() => {
    $("#loadChatFile").on("click", async function () {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const { dialog } = require("@electron/remote");

        const fileOptions: OpenDialogOptions = {
            title: "Open Chat",
            properties: ["openFile"],
            filters: [
                { name: "Chat", extensions: ["json"] },
                { name: "All Files", extensions: ["*"] }
            ]
        };

        const value = await dialog.showOpenDialog(fileOptions);

        // Make sure we got a file
        if (!value.filePaths[0]) return;

        new ChatManager(value.filePaths[0]);
    });
});

class ChatManager {
    lastPlayerTime = 0;

    chatStreamer: type_commentsV1["streamer"];
    chatMessages: type_commentsV2["comments"]["edges"];
    chatResources: type_commentsV1["resources"];

    constructor(chatPath: string) {
        const file = readFileSync(chatPath, "utf8");
        const chatJSON = JSON.parse(file);

        let chatType: "V1" | "V2" = "V1";

        // id and creator exist in V2 but not V1
        const chatTest: { "id"?: string, "creator"?: { "id": string } } = chatJSON;
        if (chatTest.id && chatTest.creator) chatType = "V2";

        // If V2, we basically just load right in
        if (chatType === "V2") {
            const chat: type_commentsV2 = chatJSON;

            this.chatStreamer = {
                "id": chat.creator.id
            };
            this.chatMessages = chat.comments.edges;

            // TODO: V2 cached Resources
            this.chatResources = { "badges": undefined, "cheermotes": undefined, "emotes": undefined };

            // For V1, we convert to V2
        } else {
            const chat: type_commentsV1 = chatJSON;
            this.chatStreamer = chat.streamer;
            this.chatMessages = [];

            // Iterate through messages
            for (const msg of chat.comments) {
                // Skip messages without fragments
                if (!msg.message.fragments) continue;

                const fragments: type_commentsV2["comments"]["edges"][0]["node"]["message"]["fragments"] = [];

                // Iterate and convert fragments
                for (const frag of msg.message.fragments) {
                    let emote: type_commentsV2["comments"]["edges"][0]["node"]["message"]["fragments"][0]["emote"] = undefined;
                    if (frag.emoticon) {
                        emote = {
                            "emoteID": frag.emoticon.emoticon_id,
                            "from": +frag.emoticon.emoticon_set_id,
                            // Not a true replacement
                            "id": `${frag.emoticon.emoticon_id};${frag.emoticon.emoticon_set_id}`,
                        };
                    }
                    fragments.push({
                        emote,
                        "text": frag.text,
                    });
                }

                const userBadges: type_commentsV2["comments"]["edges"][0]["node"]["message"]["userBadges"] = [];

                // Iterate and convert badges
                for (const badge of msg.message.user_badges ?? []) {
                    userBadges.push({
                        // TODO: Actually do this
                        "id": "",
                        "setID": badge._id ?? "",
                        "version": badge.version ?? "",
                    });
                }

                // Convert main message
                this.chatMessages.push({
                    "node": {
                        "commenter": {
                            "displayName": msg.commenter.display_name,
                            "id": msg.commenter._id,
                            "login": msg.commenter.name,
                        },
                        "contentOffsetSeconds": msg.content_offset_seconds,
                        "message": {
                            fragments,
                            userBadges,
                            "userColor": msg.message.user_color,
                        }
                    }
                });
            }
            // this.chatResources = chat.resources;
            // TODO: This breaks resources on TwitchChatDownloader files
            this.chatResources = { "badges": undefined, "cheermotes": undefined, "emotes": undefined };
        }


        // Update chat every second
        setInterval(() => {
            // Don't update if player hasn't advanced
            if (this.lastPlayerTime === playerTime) return;
            this.lastPlayerTime = playerTime;

            this.updateChat();
        }, 1000);
    }

    async updateChat() {
        const chatDiv: string[] = [];
        for (const message of this.chatMessages) {
            // Skip messages that are too new
            if (message.node.contentOffsetSeconds > playerTime) continue;

            // Some messages have no commenter for some reason, just skip
            if (!message.node.commenter)
                continue;

            const messageBody: string[] = [];
            for (const frag of message.node.message.fragments) {
                if (frag.emote) {
                    const src = loadFirstPartyEmote(frag.emote.emoteID, this.chatResources);
                    messageBody.push(`<img src="${src}" class="chatImage" style="width: 32px; height: 32px">`);
                    continue;
                }
                for (const word of frag.text.split(" ")) {
                    // V2 Does not seem to have a way to check if a message actually had cheer
                    // or if someone just typed "Cheer100" into chat
                    // if (message.message.bits_spent) {
                    const bits = await loadCheer(word, this.chatStreamer.id, this.chatResources);
                    if (bits) {
                        messageBody.push(`<img src="${bits}" class="chatImage" style="width: 32px; height: 32px">`);
                        continue;
                    }
                    // }
                    const src = await loadThirdPartyEmote(word, this.chatStreamer.id, this.chatResources);
                    if (src) {
                        messageBody.push(`<img src="${src}" class="chatImage" style="width: 32px; height: 32px">`);
                        continue;
                    }
                    messageBody.push(word);
                }
            }

            const badges: string[] = [];
            for (const badge of message.node.message.userBadges ?? []) {
                if (!badge.setID || !badge.version) continue;
                const src = await loadBadge(badge.setID, +badge.version, this.chatStreamer.id, this.chatResources);
                badges.push(`<img src="${src}" class="chatImage">`);
            }

            // Push message to div array
            chatDiv.push(`<div class="chatMessage">${badges.join("")}<font color="${message.node.message.userColor}">${message.node.commenter.displayName}</font>: ${messageBody.join(" ")}</div>`);
        }

        // Max of 500 messages in chat
        if (chatDiv.length > 500) chatDiv.splice(0, chatDiv.length - 500);

        // Set the chat div to have the contents of the array
        $("#chatText").html(chatDiv.join(""));

        // Quick way to make chat scroll
        $("#chatText").scrollTop(99999999999);
    }
}
