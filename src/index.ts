import { app, BrowserWindow, BrowserWindowConstructorOptions } from "electron";
import path from "path";
import { enable, initialize } from "@electron/remote/main";

// Initialize electron remote
initialize();

// Create window on ready
app.on("ready", createWindow);

// Quit when windows are closed
app.on("window-all-closed", app.quit);

function createWindow() {
    // Window options
    const window: BrowserWindowConstructorOptions = {
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        },
        title: "VOD Replay"
    };

    // Create window
    const mainWindow = new BrowserWindow(window);

    // Enable remote module
    enable(mainWindow.webContents);

    // Load html
    mainWindow.loadURL(path.join("file://", __dirname, "/index.html"));
}
