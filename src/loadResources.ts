import fetch from "node-fetch";
import { t_badgeJSON } from "./types/badges";
import { type_commentsV1 } from "./types/commentsV1";

// Data for making API requests
const clientId = "kimne78kx3ncx6brgo4mv6wki5h1ko";
const reqHeaders = { headers: { "Accept": "application/vnd.twitchtv.v5+json", "Client-ID": clientId } };

let badges: t_badgeJSON, channelBadges: t_badgeJSON;
let cheerSearch: Map<string, string>;
let emoteSearch: Map<string, { "id": string, "url": string, "format": string }>;

// Loads badges from embedded resources or Twitch
export async function loadBadge(id: string, version: number, channelId: string, resources: type_commentsV1["resources"]): Promise<string> {
    // If badges are an embedded resource
    if (resources.badges) {
        // Get embedded version
        const data = resources.badges[id + version];
        // Return base64 data
        if (data) return new Promise((resolve) => { resolve(`data:image/png;base64,${data}`); });
    }

    // If we haven't already, get badge manifests from Twitch
    if (!badges) badges = await (await fetch("https://badges.twitch.tv/v1/badges/global/display")).json();
    if (!channelBadges) channelBadges = await (await fetch(`https://badges.twitch.tv/v1/badges/channels/${channelId}/display`)).json();

    let badgeUrl: string;

    // Check if channel badges satisfies id, if not use global id
    if (channelBadges.badge_sets[id])
        badgeUrl = channelBadges.badge_sets[id].versions[version]?.image_url_1x; // Need to work out a fallback to global for cheer badges
    else
        badgeUrl = badges.badge_sets[id].versions[version].image_url_1x;

    return badgeUrl;
}

// Loads cheermotes from embedded resources or Twitch
export async function loadCheer(id: string, channelId: string, resources: type_commentsV1["resources"]): Promise<undefined | string> {
    // If badges are an embedded resource
    if (resources.cheermotes) {
        // Get embedded version
        const data = resources.cheermotes[id]?.data;
        // Return base64 data
        if (data) return new Promise((resolve) => { resolve(`data:image/png;base64,${data}`); });
        return;
    }

    // TODO: This endpoint no longer exists
    // If we haven't already, get cheer manifest from Twitch and parse it
    // if (!cheerSearch) {
    //     const cheer = await (await fetch(`https://api.twitch.tv/kraken/bits/actions?channel_id=${channelId}`, reqHeaders)).json();

    //     // Make new map to parse to
    //     cheerSearch = new Map();

    //     for (let i = 0; i < cheer.actions.length; i++) {
    //         const cheermote = cheer.actions[i];
    //         for (let x = 0; x < cheermote.tiers.length; x++) {
    //             const tier = cheermote.tiers[x];

    //             // Skip cheermotes we already know
    //             if (cheerSearch.has(`${cheermote.prefix}${tier.id}`)) continue;

    //             if (cheermote.states.includes("animated")) {
    //                 cheerSearch.set(`${cheermote.prefix}${tier.id}`, tier.images.dark.animated[1]);
    //             } else if (cheermote.states.includes("static")) {
    //                 cheerSearch.set(`${cheermote.prefix}${tier.id}`, tier.images.dark.static[1]);
    //             }
    //         }
    //     }
    // }

    // // Get url for id
    // const url = cheerSearch.get(id);
    // return url;
}

// Loads emotes from embedded resources or Twitch
export function loadFirstPartyEmote(id: string, resources: type_commentsV1["resources"]): string {
    // If emotes are an embedded resource
    if (resources.emotes) {
        // Get embedded version
        const data = `data:image/png;base64,${resources.emotes.firstParty[id].data}`;
        // Return base64 data
        return data;
    }

    // Emote endpoint
    const url = `https://static-cdn.jtvnw.net/emoticons/v2/${id}/default/dark/1.0`;
    return url;
}

// Loads emotes from embedded resources or BTTV/FFZ
export async function loadThirdPartyEmote(id: string, channelId: string, resources: type_commentsV1["resources"]): Promise<undefined | string> {
    // If emotes are an embedded resource
    if (resources.emotes) {
        // Get embedded version
        const data = resources.emotes.thirdParty[id];
        // Return base64 data
        if (data) return new Promise((resolve) => { resolve(`data:image/${data.format};base64,${data.data}`); });
        return;
    }

    // If we haven't already, get emote manifests from BTTV and parse them
    if (!emoteSearch) {
        emoteSearch = new Map();

        // Download ThirdParty emote manifests
        const bttvGlobal = await (await fetch("https://api.betterttv.net/3/cached/emotes/global")).json();
        const bttvChannel = await (await fetch(`https://api.betterttv.net/3/cached/users/twitch/${channelId}`)).json();
        const ffzGlobal = await (await fetch("https://api.betterttv.net/3/cached/frankerfacez/emotes/global")).json();
        const ffzChannel = await (await fetch(`https://api.betterttv.net/3/cached/frankerfacez/users/twitch/${channelId}`)).json();

        // Remap global bttv emotes
        for (let i = 0; i < bttvGlobal.length; i++)
            emoteSearch.set(bttvGlobal[i].code, { "id": bttvGlobal[i].id, "url": `https://cdn.betterttv.net/emote/${bttvGlobal[i].id}/1x`, "format": bttvGlobal[i].imageType });

        // Remap global ffz emotes
        for (let i = 0; i < ffzGlobal.length; i++)
            emoteSearch.set(ffzGlobal[i].code, { "id": ffzGlobal[i].id.toString(), "url": ffzGlobal[i].images["1x"], "format": ffzGlobal[i].imageType });

        // Remap channel bttv emotes
        if (bttvChannel.channelEmotes)
            for (let i = 0; i < bttvChannel.channelEmotes.length; i++)
                emoteSearch.set(bttvChannel.channelEmotes[i].code, { "id": bttvChannel.channelEmotes[i].id, "url": `https://cdn.betterttv.net/emote/${bttvChannel.channelEmotes[i].id}/1x`, "format": bttvChannel.channelEmotes[i].imageType });

        // Remap shared bttv emotes
        if (bttvChannel.sharedEmotes)
            for (let i = 0; i < bttvChannel.sharedEmotes.length; i++)
                emoteSearch.set(bttvChannel.sharedEmotes[i].code, { "id": bttvChannel.sharedEmotes[i].id, "url": `https://cdn.betterttv.net/emote/${bttvChannel.sharedEmotes[i].id}/1x`, "format": bttvChannel.sharedEmotes[i].imageType });

        // Remap channel ffz emotes
        if (ffzChannel)
            for (let i = 0; i < ffzChannel.length; i++)
                emoteSearch.set(ffzChannel[i].code, { "id": ffzChannel[i].id.toString(), "url": ffzChannel[i].images["1x"], "format": ffzChannel[i].imageType });
    }

    // Get data for id
    const data = emoteSearch.get(id);

    // Return url
    if (data) return data.url;
}
