/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/ban-ts-comment */
import { OpenDialogOptions } from "electron";

$(() => {
    $("#loadMediaFile").on("click", async function () {
        const { dialog } = require("@electron/remote");

        const fileOptions: OpenDialogOptions = {
            title: "Open Media",
            filters: [
                { name: "VOD", extensions: ["MP4", "MKV", "WEBM"] },
                { name: "All Files", extensions: ["*"] }
            ],
            properties: [
                "openFile"
            ]
        };

        const value = await dialog.showOpenDialog(fileOptions);

        if (!value.filePaths[0]) return;

        $("#playerDiv").html(`<video id="player" autoplay controls><source src="${value.filePaths[0]}", type="video/mp4"></video>`);

        $("#player").on("timeupdate", function (event) {
            // @ts-ignore | currentTime DOES exist on this
            playerTime = event.currentTarget.currentTime;
        });
    });

    $("#loadYouTubeVideo").on("click", function () {
        // return if no id is given
        if (!$("#YTVideoID").val()) return;

        // Load YouTube script
        const tag = document.createElement("script");
        tag.src = "https://www.youtube.com/iframe_api";
        const firstScriptTag = document.getElementsByTagName("script")[0];
        firstScriptTag.parentNode?.insertBefore(tag, firstScriptTag);
    });
});

//#region YouTube player
let player: YT.Player, updateInterval: NodeJS.Timeout;

// YouTube's script will call this when ready
function onYouTubeIframeAPIReady() {
    // Get video id from the textbox
    let id: string[] | string | undefined = $("#YTVideoID").val()?.toString().split("?v=");
    if (!id) return;

    if (id.length > 1)
        // Remove the beginning of the url and any additional arguments on the end
        id = id[id.length - 1].split("&")[0];
    else {
        // If the url is using the YouTube url shortener
        id = id[0].split("/");
        id = id[id.length - 1];
    }

    // Make the YouTube player
    player = new YT.Player("playerDiv", {
        videoId: id,
        events: {
            "onReady": onReady
        }
    });
}

// The player calls this when ready
function onReady() {
    // Play then video
    player.playVideo();

    // Create the update interval
    if (!updateInterval)
        updateInterval = setInterval(() => {
            // @ts-ignore | Defined in chat.ts
            playerTime = player.getCurrentTime();
        }, 500);
}
//#endregion
